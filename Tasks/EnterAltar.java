package Tasks;

import Main.FRunecrafter;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class EnterAltar extends Task {
    @Override
    public boolean validate() {
        return FRunecrafter.craftingArea != null && FRunecrafter.craftingArea.getAltar().contains(Players.getLocal()) && !FRunecrafter.craftingArea.getInsideAltar().contains(Players.getLocal());
    }

    @Override
    public int execute() {
        FRunecrafter.currentState = "Entering Altar";
        SceneObjects.getNearest(FRunecrafter.craftingArea.getRuinesId()).interact(x -> true);
        Time.sleepUntil(() -> !Players.getLocal().isMoving(), 3000, 5000);
        return 300;
        }
}
