package Tasks;

import Main.FRunecrafter;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class LeaveAltar extends Task {
    @Override
    public boolean validate() {
        return FRunecrafter.craftingArea != null && FRunecrafter.craftingArea.getInsideAltar().contains(Players.getLocal()) && !Inventory.isFull();
    }

    @Override
    public int execute() {
        FRunecrafter.currentState = "Leaving Altar";
        FRunecrafter.runesCrafted = Inventory.getCount(true, FRunecrafter.craftingArea.getRunes());
        SceneObjects.getNearest(FRunecrafter.craftingArea.getPortalId()).interact(x -> true);
        Time.sleepUntil(() -> !Players.getLocal().isMoving(), 3000, 5000);
        return 330;
    }
}
