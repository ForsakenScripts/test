package Tasks;

import Main.FRunecrafter;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;

public class Banking extends Task {
    private static final Predicate<Item> TALISMAN = item -> item.getName().contains("talisman");

    @Override
    public boolean validate() {
        return FRunecrafter.craftingArea != null && !Inventory.isFull() && FRunecrafter.craftingArea.getBank().contains(Players.getLocal() );
    }

    @Override
    public int execute() {
        FRunecrafter.currentState = "Banking Runes";
        if (Inventory.contains(FRunecrafter.craftingArea.getRunes()) && Bank.open()) {
            Time.sleepUntil(() -> Bank.isOpen(), 1200, 4000);
            Bank.depositAllExcept(TALISMAN);
            Time.sleepUntil(() -> Inventory.isEmpty(), 1000, 2000);
            if(!Inventory.contains(FRunecrafter.essence)) {
                Bank.withdrawAll(FRunecrafter.essence);
                Time.sleepUntil(() -> Inventory.isFull(), 2000, 1500);
            }
        }
        return 300;
    }
}
