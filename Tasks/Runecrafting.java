package Tasks;

import Main.FRunecrafter;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;

public class Runecrafting extends Task {

    public static final Predicate<Item> ESSENCE = item -> item.getName().contains("essence");

    @Override
    public boolean validate() {
        return FRunecrafter.craftingArea != null && FRunecrafter.craftingArea.getInsideAltar().contains(Players.getLocal()) && Inventory.isFull();
    }

    @Override
    public int execute() {
        FRunecrafter.currentState = "Crafting Runes";
        SceneObjects.getNearest(FRunecrafter.craftingArea.getCraftId()).interact(x -> true);
        Time.sleepUntil(() -> !Players.getLocal().isMoving(), 3000, 5000);
        return 330;
    }
}
