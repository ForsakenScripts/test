package Tasks;

import Main.FRunecrafter;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.concurrent.ThreadLocalRandom;

public class Traverse extends Task {

    private int toggleNextRun = 20;

    @Override
    public boolean validate() {
        return FRunecrafter.craftingArea != null && traverseToBank() || traverseToAltar();
    }

    @Override
    public int execute() {
        FRunecrafter.currentState = "Walking";
        checkRunEnergy();
        Movement.walkTo(traverseToBank() ? FRunecrafter.craftingArea.getBank().getCenter() : FRunecrafter.craftingArea.getAltar().getCenter());
        Time.sleepUntil(() -> !Players.getLocal().isMoving(), 1000, 2000);
        return 300;
    }

    private boolean traverseToBank() {
            return !Inventory.isFull() && !FRunecrafter.craftingArea.getBank().contains(Players.getLocal()) && Inventory.contains(FRunecrafter.craftingArea.getRunes()) && !FRunecrafter.craftingArea.getInsideAltar().contains(Players.getLocal());
    }

    private boolean traverseToAltar(){
            return Inventory.isFull() && !FRunecrafter.craftingArea.getAltar().contains(Players.getLocal()) && Inventory.contains(Runecrafting.ESSENCE) && !FRunecrafter.craftingArea.getInsideAltar().contains(Players.getLocal());
    }

    public void checkRunEnergy(){
        if(Movement.getRunEnergy() > toggleNextRun && !Movement.isRunEnabled()){
            Movement.toggleRun(true);
            toggleNextRun = ThreadLocalRandom.current().nextInt(20, 30 + 1);
        }
    }
}
