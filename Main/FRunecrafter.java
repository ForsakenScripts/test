package Main;

import Enums.Altars;
import GUI.FRCGUI;
import Paint.PaintBase;
import Utils.SkillTracker;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

@ScriptMeta(developer = "WaRp", desc = "Kinda Magic", version = 0.2 ,name = "FRunecrafting", category = ScriptCategory.RUNECRAFTING)


public class FRunecrafter extends TaskScript implements RenderListener {

    private static final Task[] TASKS = {new Tasks.Banking(), new Tasks.Traverse(), new Tasks.Runecrafting(), new Tasks.EnterAltar(), new Tasks.LeaveAltar()};
    public static Altars craftingArea;
    public static String essence;
    public static int runesCrafted = 0;
    public static String currentState = "Script Setup";

    private PaintBase paintBase;

    @Override
    public void onStart() {
        new FRCGUI().setVisible(true);
        paintBase = new PaintBase();
        SkillTracker.setStartTime();
        SkillTracker.addTracker(Skill.RUNECRAFTING);
        submit(TASKS);
    }

    @Override
    public void notify(RenderEvent e) { paintBase.notify(e); }
}
