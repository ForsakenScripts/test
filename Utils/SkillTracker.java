package Utils;

import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;

import java.util.HashMap;
import java.util.Map;

public class SkillTracker {
    private static final Map<Skill, Tracker> TRACKER_MAP = new HashMap<>();
    private static StopWatch timer;

    public static void setStartTime() {
        timer = StopWatch.start();
    }

    public static void addTracker(Skill skill) {
        TRACKER_MAP.putIfAbsent(skill, new Tracker(skill));
    }

    public static int getExperienceGained(Skill skill) {
        if(!TRACKER_MAP.containsKey(skill)) return 0;
        return TRACKER_MAP.get(skill).getExperienceGained();
    }

    public static int getExperiencePerHour(Skill skill) {
        if(!TRACKER_MAP.containsKey(skill)) return 0;
        return (int)timer.getHourlyRate(getExperienceGained(skill));
    }

    public static int getMoneyPerHour(int price) {
        return (int)timer.getHourlyRate(price);
    }

    public static int getLevelsGained(Skill skill) {
        if(!TRACKER_MAP.containsKey(skill)) return 0;
        return TRACKER_MAP.get(skill).getLevelsGained();
    }

    public static int getCurrentLevel(Skill skill) {
        if(!TRACKER_MAP.containsKey(skill)) return 0;
        return Skills.getCurrentLevel(skill);
    }

    public static int getExperienceRemaining(Skill skill) {
        return Skills.getExperienceToNextLevel(skill);
    }

    public static String getElapsedString() {
        return timer.toElapsedString();
    }

    public static int getPercentToLvl(Skill skill) {
        int currentXp = Skills.getExperience(skill);
        int currentSkillLvl = Skills.getLevel(skill);
        int xpNextLvl = Skills.getExperienceAt(currentSkillLvl);
        int xpAtLvl = Skills.getExperienceAt(currentSkillLvl + 1);
        return (((xpNextLvl - currentXp) * 100) / (xpNextLvl - xpAtLvl));

    }

    private static class Tracker {
        Skill skill;
        int startingExperience;
        int startingLevel;

        Tracker(Skill skill) {
            this.startingExperience = Skills.getExperience(skill);
            this.startingLevel = Skills.getVirtualLevel(skill);
            this.skill = skill;
        }

        int getExperienceGained() {
            return Skills.getExperience(this.skill) - this.startingExperience;
        }

        int getLevelsGained() {
            return Skills.getVirtualLevel(this.skill) - this.startingLevel;
        }
    }
}
