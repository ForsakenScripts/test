package Paint;

import Main.FRunecrafter;
import Utils.SkillTracker;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;

import java.awt.*;

public class PaintBase implements RenderListener {

    private final Color rectangFill = new Color(204, 0 , 0, 150);
    private final Color rectangEmpty = new Color(255, 0, 0, 50);
    private final Color rectangBorder = new Color(0, 0, 0, 200);
    private final Color textColor = new Color(255,69,0);
    private final Color percentageColor = new Color(255,250,250);
    private final BasicStroke stroke = new BasicStroke(1);

    @Override
    public void notify(RenderEvent e){
        Graphics g = e.getSource();
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        double doublePercent = SkillTracker.getPercentToLvl(Skill.WOODCUTTING) * 4.71;
        int width = (int) doublePercent;
        g2.setColor(rectangFill);
        g2.fillRoundRect(14, 432, width, 20, 16, 16);
        g2.setColor(rectangBorder);
        g2.setStroke(stroke);
        g2.drawRoundRect(14, 432, 471, 20, 16, 16);
        g2.setColor(rectangEmpty);
        g2.fillRoundRect(14, 432, 471, 20, 16, 16);
        g.setColor(textColor);
        g.drawString("FRuneCrafting V0.2", 10, 15);
        g.drawString("Runtime: " + SkillTracker.getElapsedString(), 10, 30);
        g.drawString("Current State: " + FRunecrafter.currentState, 10, 45);
        g2.setColor(percentageColor);
        g.drawString("Exp Gained: " + SkillTracker.getExperienceGained(Skill.RUNECRAFTING) + " - Hourly: " + SkillTracker.getExperiencePerHour(Skill.RUNECRAFTING) + " - Runes crafted: " + FRunecrafter.runesCrafted + " - Level " + Skills.getVirtualLevel(Skill.RUNECRAFTING) + "(" + SkillTracker.getLevelsGained(Skill.RUNECRAFTING) + ")", 18, 447);
    }
}
