package Enums;

import org.rspeer.runetek.api.movement.position.Area;

public enum Altars {

    AIR(Area.rectangular(2977, 3298, 2994, 3281), Area.rectangular(2837, 4843, 2852, 4824), Area.rectangular(3009, 3358, 3018, 3355), 556, 34813, 34760, 34748),
    MIND(Area.rectangular(2987, 3506, 2977, 3517), Area.rectangular(2769, 4853, 2799, 4821), Area.rectangular(2943, 3371, 2949, 3367), 558, 34814, 34761, 34749),
    WATER(Area.rectangular(3187, 3155, 3177, 3168), Area.rectangular(2728, 4828, 2707, 4843), Area.rectangular(3092, 3246, 3097, 3240), 555, 34815, 34762, 34750),
    EARTH(Area.rectangular(3309, 3470, 3294, 3486), Area.rectangular(2676, 4819, 2636, 4856), Area.rectangular(3250, 3424, 3257, 3419), 557, 34816, 34763, 34751),
    FIRE(Area.rectangular(3317, 3250, 3306, 3258), Area.rectangular(2571, 4852, 2596, 4829), Area.rectangular(3269, 3171, 3272, 3163), 554, 34817, 34764, 34752),
    BODY(Area.rectangular(3058, 3436, 3048, 3447), Area.rectangular(2514, 4847, 2532, 4831), Area.rectangular(3180, 3443, 3185, 3434), 559, 34818, 34765, 34753);

    private final Area altar, insideAltar ,bank;
    private final int runes, ruinesId, craftId, portalId;

Altars(Area altar, Area insideAltar, Area bank, int runes, int ruinesId, int craftId, int portalId){
    this.altar = altar;
    this.insideAltar = insideAltar;
    this.bank = bank;
    this.runes = runes;
    this.ruinesId = ruinesId;
    this.craftId = craftId;
    this.portalId = portalId;
}

    public Area getAltar() {
        return altar;
    }
    public Area getBank() {
        return bank;
    }
    public Area getInsideAltar() {
        return insideAltar;
    }
    public int getRunes() {
        return runes;
    }
    public int getRuinesId() { return ruinesId; }
    public int getCraftId() { return craftId; }
    public int getPortalId() { return portalId; }
}
