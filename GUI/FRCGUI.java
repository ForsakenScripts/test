package GUI;

import Enums.Altars;
import Main.FRunecrafter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FRCGUI extends JFrame {

    private JComboBox altarCombo;
    private JPanel panelMain;
    private JComboBox essenceCombo;
    private JButton startButton;
    String [] essence = {"Pure essence", "Rune essence"};

    public FRCGUI(){
        super("FRunecrafter V0.3");
        setLayout(new FlowLayout());

        startButton = new JButton("Start");
        altarCombo = new JComboBox(Altars.values());
        essenceCombo = new JComboBox(essence);
        panelMain = new JPanel();
        add(panelMain);
        add(altarCombo);
        add(essenceCombo);
        add(startButton);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        pack();

        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FRunecrafter.craftingArea = (Altars) altarCombo.getSelectedItem();
                FRunecrafter.essence = (String) essenceCombo.getSelectedItem();
                setVisible(false);
            }
        });
    }
}
